scalaVersion := "2.10.4"

lazy val commonSettings = Seq(
  version := "0.1-SNAPSHOT",
  organization := "com.nam",
  test in assembly := {}
)

libraryDependencies ++= Seq(
  "commons-codec" % "commons-codec" % "1.9",
  "com.databricks" % "spark-csv_2.10" % "1.5.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
  "org.scala-lang" % "scala-library" % "2.10.4",
  "com.amazonaws" % "aws-java-sdk" % "1.11.46",
  "org.skife.com.typesafe.config" % "typesafe-config" % "0.3.0",
  "org.apache.flink" % "flink-connector-elasticsearch2_2.10" % "1.2.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
)

/*

"org.apache.spark" % "spark-sql_2.10" % "1.6.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
"org.apache.spark" % "spark-core_2.10" % "1.6.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
"org.apache.spark" % "spark-sql_2.10" % "1.6.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
"org.apache.hadoop" % "hadoop-common" % "2.7.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
"org.apache.spark" % "spark-yarn_2.10" % "1.6.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
 "org.apache.flink" % "flink-connector-elasticsearch2_2.10" % "1.2.0" exclude ("org.apache.hadoop","hadoop-yarn-server-web-proxy"),
*/
