package com.nam.mixpanel

object EventProcessor {
  val EVENT_KEY = "event"

  def getEventDataWithSchema(singleEventData: List[Map[String, Object]]): List[Map[String, Object]] = {
    val uniqueProperties = getUniqueProps(singleEventData)

    singleEventData.map { event =>
      val properties = getEventProperties(event)
      val difference = uniqueProperties -- (properties.keySet)
      properties ++ difference.map(i => i -> null).toMap
    }

  }

  def getUniqueProps(events: List[Map[String, Object]]): Set[String] = {
    val keysSet = events.map { s =>
      val propsMap = getEventProperties(s)
      propsMap.keySet
    }
    keysSet.reduce(_ ++ _)
  }

  private def getEventProperties(s: Map[String, Object]) = {
    s.get("properties").get.asInstanceOf[Map[String, Object]]
  }

}
