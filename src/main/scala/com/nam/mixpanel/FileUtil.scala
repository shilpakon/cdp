package com.nam.mixpanel

import java.io._

import com.typesafe.config.ConfigFactory

object FileUtil {

  val config = ConfigFactory.load

  def writeFileToPath (mixpanelResponse: Iterator[String]) = {

    val path = "mixpanel-data"

    val writer = new PrintWriter(new File(s"$path/mixpanel-all-events-data.json"))

    mixpanelResponse.foreach( line => {
      writer.write(line)
      writer.write("\r\n")
    })

    writer.close()
  }

  def writeDataToTempFile (stream : Iterator[String], prefix : String, suffix : String) : File = {
    val formattedEventName = prefix.replace(":", "").replace(" ","_")

    val tempFile = File.createTempFile(formattedEventName, suffix)

    val writer = new PrintWriter(tempFile)

    stream.foreach( line => {
      writer.write(line)
      writer.write("\r\n")
    })

    writer.close()
    tempFile.deleteOnExit()
    tempFile
  }

  def getStringIterator(header : Set[String], rows : List[Map[String, Object]]) : Iterator[String] = {
    val csvHeader  = List(header.mkString("~"))
    val data =  rows.map{s => s.values.toArray.mkString("~") }
    val finalData = (csvHeader ::: data).toIterator
    finalData
  }

}
