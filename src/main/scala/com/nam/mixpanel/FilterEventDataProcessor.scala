package com.nam.mixpanel

import java.io.InputStream

import com.typesafe.config.ConfigFactory

import scala.io.Source
import scala.util.{Failure, Success}
import scala.util.parsing.json.JSON


object FilterEventDataProcessor {

  val config = ConfigFactory.load
  val EVENT_KEY = "event"

  def main(args: Array[String]): Unit = {
    val toDate = args(0)
    val directoryPath = toDate.replace("-", "/")

    val interestedEventNames = config.getString("mixpanelInterestedEvents").split(",")

    val mixpanelResponse = S3Client.getObject(directoryPath)
    mixpanelResponse match {
      case Success(fileContent) => {

        val mixpanelData = Source.fromInputStream(fileContent.asInstanceOf[InputStream], "UTF-8").getLines()

        val eventsData = mixpanelData.map(JSON.parseFull(_).get.asInstanceOf[Map[String, Object]]).toList

        processEvents(eventsData, interestedEventNames, directoryPath)
      }
      case Failure(e) => {
        print("s3 file download failed")
      }
    }
  }

  def processEvents(eventsData: List[Map[String, Object]], interestedEventNames: Array[String], directoryPath: String): Unit = {
    for (eventName <- interestedEventNames) {
      val singleEventData = eventsData.filter(_.get(EVENT_KEY).get == eventName)
      if (singleEventData.size != 0){

        val propertiesWithSchema = EventProcessor.getEventDataWithSchema(singleEventData)
        val dataInDelimitedFormat = FileUtil.getStringIterator(EventProcessor.getUniqueProps(singleEventData), propertiesWithSchema)

        val tempFile = FileUtil.writeDataToTempFile( dataInDelimitedFormat, eventName, ".csv")

        val formattedEventName = eventName.replace(":", "").replace(" ","_")
        val response = S3Client.storeFileToS3(s"${config.getString("s3MixpanelRootFolder")}/raw/$directoryPath/$formattedEventName/$formattedEventName.json", tempFile)
        response match {
          case Success(_) => {
            println(s"$formattedEventName File is written to S3 successfully")
          }
          case Failure(e) => {
            println(s"$formattedEventName File write to S3 is unsuccessful:  ${e.getMessage}")
          }
        }
      }
    }
  }
}
