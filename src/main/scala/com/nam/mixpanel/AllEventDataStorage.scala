package com.nam.mixpanel

import com.typesafe.config.ConfigFactory

import scala.util.{Failure, Success}

object AllEventDataStorage {

  def main (args: Array[String]): Unit = {
    val fromDate = args(0)
    val toDate = args(1)
    val toDateFolder = toDate.replace("-", "/")
    val config = ConfigFactory.load

    val mixpanelResponse = MixpanelApiClient.getRawEventData(fromDate, toDate)
    if (mixpanelResponse.isLeft) {
      val mixpanelData = mixpanelResponse.left.get

      val tempFile = FileUtil.writeDataToTempFile(mixpanelData, "mixpanel", ".json")

      val response = S3Client.storeFileToS3(s"${config.getString("s3MixpanelRootFolder")}/raw-input/$toDateFolder/mixpanel-all-data.json", tempFile)
      response match {
        case Success(_) => {
          println("File is written successfully")
        }
        case Failure(e) => {
          println("File write is unsuccessful:  " + e.getMessage)
        }
      }
    } else {
      //Log the response code
      print(mixpanelResponse.right.getOrElse())
    }

  }
}
