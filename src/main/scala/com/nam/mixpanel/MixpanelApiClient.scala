package com.nam.mixpanel

import java.net.URL

import com.typesafe.config.ConfigFactory
import javax.net.ssl.HttpsURLConnection
import org.apache.commons.codec.binary.Base64

import scala.io.Source

object MixpanelApiClient {

  val config = ConfigFactory.load
  val BASIC = "Basic"
  val AUTHORIZATION = "Authorization"
  val MIXPANEL_BASE_URL = config.getString("mixpanelBaseUrl")
  val MIXPANEL_USERNAME = config.getString("mixpanelUsername")
  val MIXPANEL_PASSWORD = config.getString("mixpanelPassword")

  def getRawEventData (fromDate: String, toDate: String): Either[Iterator[String], Int] = {
    val eventUrl = s"$MIXPANEL_BASE_URL/export/?from_date=$fromDate&to_date=$toDate"
    getApiResponse(eventUrl)
  }


  private def getApiResponse (eventUrl: String): Either[Iterator[String], Int] = {
    val url = new URL(eventUrl)
    val con: HttpsURLConnection = url.openConnection().asInstanceOf[HttpsURLConnection]
    con.setRequestMethod("GET")
    con.setRequestProperty(AUTHORIZATION, getHeader(MIXPANEL_USERNAME))
    val responseCode = con.getResponseCode
    if (responseCode != 200)
      Right(responseCode)
    else
      Left(Source.fromInputStream(con.getInputStream, "UTF-8").getLines())
  }

  private def encodeCredentials (username: String, password: String): String = {
    new String(Base64.encodeBase64String((username + ":" + password).getBytes));
  }

  private def getHeader (username: String): String =
    BASIC + " " + encodeCredentials(username, "");

}
