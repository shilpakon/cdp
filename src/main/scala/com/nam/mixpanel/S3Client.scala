package com.nam.mixpanel

import java.io.File

import com.amazonaws.{AmazonClientException, AmazonServiceException}
import com.amazonaws.services.s3.AmazonS3Client
import com.typesafe.config.ConfigFactory

import scala.util.{Failure, Success, Try}

object S3Client {
  val amazonS3Client = new AmazonS3Client()
  val config = ConfigFactory.load

  def storeFileToS3 (keyString: String, file: File): Try[Any] = {
    try {
      amazonS3Client.putObject(config.getString("s3BucketName"), keyString, file)
      Success("success")
    }
    catch {
      case ace: AmazonClientException => { Failure(ace) }
      case ase: AmazonServiceException => { Failure(ase) }
    }

  }

  def getObject (directoryPath: String): Try[Any] = {
    try {

      val keyForS3 = s"${config.getString("s3MixpanelRootFolder")}/raw-input/$directoryPath/mixpanel-all-data.json"
      println(keyForS3)
      val content = amazonS3Client.getObject(config.getString("s3BucketName"), s"${config.getString("s3MixpanelRootFolder")}/raw-input/$directoryPath/mixpanel-all-data.json").getObjectContent()

      Success(content)
    }
    catch {
      case ace: AmazonClientException => { Failure(ace) }
      case ase: AmazonServiceException => { Failure(ase) }
    }
  }

}
